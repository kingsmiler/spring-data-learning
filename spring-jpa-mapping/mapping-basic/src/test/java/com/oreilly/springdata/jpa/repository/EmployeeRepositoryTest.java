
package com.oreilly.springdata.jpa.repository;

import com.oreilly.springdata.jpa.entity.Employee;
import com.oreilly.springdata.jpa.entity.EmployeeType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-data.xml")
public class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository repository;

    @Test
    public void testSaveRoot() {
        Employee employee = new Employee();
        employee.setName("root");
        employee.setType(EmployeeType.CONTRACT_EMPLOYEE);
        employee.setSalary(1024);

        repository.save(employee);
    }

    @Test
    public void testFindRoot() {
        String name = "root";
        Employee employee = repository.findByName(name);

        System.out.println(employee);
    }

    @Test
    public void TestSaveLeader() {
        Employee root = new Employee();
        root.setName("root");
        root.setType(EmployeeType.CONTRACT_EMPLOYEE);
        root.setSalary(1024);

        repository.save(root);

        String name = "root";
        root = repository.findByName(name);
        Set<Employee> children = new HashSet<Employee>();
        children.add(root);

        Employee emp1 = new Employee();
        emp1.setName("emp1");
        emp1.setType(EmployeeType.CONTRACT_EMPLOYEE);
        emp1.setSalary(256);
        emp1.setParent(root);

        repository.save(emp1);

        Employee emp2 = new Employee();
        emp2.setName("emp2");
        emp2.setType(EmployeeType.CONTRACT_EMPLOYEE);
        emp2.setSalary(256);
        emp2.setParent(root);

        repository.save(emp2);
    }

    @Test
    public void TestSave3() {
        String name = "emp1";
        Employee emp1 = repository.findByName(name);
        Set<Employee> children = new HashSet<Employee>();
        children.add(emp1);

        Employee emp3 = new Employee();
        emp3.setName("emp3");
        emp3.setType(EmployeeType.CONTRACT_EMPLOYEE);
        emp3.setSalary(128);
        emp3.setParent(emp1);

        repository.save(emp3);
    }

}
