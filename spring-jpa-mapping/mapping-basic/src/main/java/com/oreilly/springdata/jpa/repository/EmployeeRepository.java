
package com.oreilly.springdata.jpa.repository;

import com.oreilly.springdata.jpa.entity.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;


public interface EmployeeRepository extends CrudRepository<Employee, Long>, QueryDslPredicateExecutor<Employee> {

    @Query("select u from Employee u where u.name = ?1")
    Employee findByName(String name);
}
