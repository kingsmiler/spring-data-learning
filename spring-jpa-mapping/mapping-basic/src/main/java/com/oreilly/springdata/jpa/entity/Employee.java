package com.oreilly.springdata.jpa.entity;

import javax.persistence.*;
import java.util.Locale;
import java.util.Set;

@Entity
@Table(name = "employee")
public class Employee extends AbstractEntity {

    @Column(name = "name", length = 32)
    private String name;

    @OneToMany(fetch=FetchType.EAGER, mappedBy="parent")
    private Set<Employee> children;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="pid")
    private Employee parent;

    @Transient
    private String convertedName;

    @Column(name = "salary")
    private long salary;

    @Column(name = "comments", length = 255)
    private String comments;

    @Basic(fetch = FetchType.LAZY)
    @Lob
    @Column(name = "PIC")
    private byte[] picture;

    private EmployeeType type;

    @Enumerated(EnumType.STRING)
    private EmployeeType previousType;

    @Temporal(TemporalType.DATE)
    private java.util.Calendar dob;

    @Temporal(TemporalType.DATE)
    @Column(name = "S_DATE")
    private java.util.Date startDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        convertedName = convertName(name);
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public byte[] getPicture() {
        return picture;
    }

    public EmployeeType getType() {
        return type;
    }

    public void setType(EmployeeType type) {
        this.previousType = this.type;
        if (this.previousType == null) {
            this.previousType = type;
        }
        this.type = type;
    }

    public EmployeeType getPreviousType() {
        return previousType;
    }

    public java.util.Calendar getDob() {
        return dob;
    }

    public void setDob(java.util.Calendar dob) {
        this.dob = dob;
    }

    public java.util.Date getStartDate() {
        return startDate;
    }

    public void setStartDate(java.util.Date startDate) {
        this.startDate = startDate;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getConvertedName() {
        return convertedName;
    }

    protected String convertName(String name) {
        // Convert to upper case Canadian...
        return name.toUpperCase(Locale.CHINESE);
    }

    public void setChildren(Set<Employee> children) {
        this.children = children;
    }

    public void setParent(Employee parent) {
        this.parent = parent;
    }

    public Set<Employee> getChildren() {
        return children;
    }

    public Employee getParent() {
        return parent;
    }

    public String toString() {
        return "Employee id: " + getId() + " name: " + getName() +
                " salary: " + getSalary() + " comments: " + getComments();
    }
}
