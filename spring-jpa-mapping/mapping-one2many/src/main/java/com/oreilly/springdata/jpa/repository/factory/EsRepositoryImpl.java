package com.oreilly.springdata.jpa.repository.factory;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QueryDslJpaRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * @author xman
 */
@SuppressWarnings("unchecked")
@NoRepositoryBean
public class EsRepositoryImpl<T, ID extends Serializable>
		extends QueryDslJpaRepository<T, ID> implements EsRepository<T, ID> {

	private EntityManager entityManager;

    /**
     * Creates a new {@link org.springframework.data.jpa.repository.support.QueryDslJpaRepository} from the given domain class and {@link javax.persistence.EntityManager}. This will use
     * the {@link SimpleEntityPathResolver} to translate the given domain class into an {@link EntityPath}.
     *
     * @param entityInformation must not be {@literal null}.
     * @param entityManager     must not be {@literal null}.
     */
    public EsRepositoryImpl(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

}
