package com.oreilly.springdata.jpa.entity;

import javax.persistence.*;
import java.util.Locale;

@Entity
@Table(name = "employee")
public class Employee extends AbstractEntity {

    @Column(name = "name", length = 32)
    private String name;

    @Column(name = "salary")
    private long salary;

    @ManyToOne
    @JoinColumn(name="dept_id")
    private Department department;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String toString() {
        return "Employee id: " + getId() + " name: " + getName() +
                " with " + getDepartment();
    }
}
