package com.oreilly.springdata.jpa.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.*;

/**
 * 多对多双向关联的员工表
 */
@Entity
@Table(name = "bi_employee")
public class BiEmployee extends AbstractEntity {
    private String name;
    private long salary;
    
    @ManyToMany
    @JoinTable(name="bi_employee_projects",
            joinColumns=@JoinColumn(name="emp_id"),
            inverseJoinColumns=@JoinColumn(name="proj_id"))
    private Collection<BiProject> projects;

    public BiEmployee() {
        projects = new ArrayList<BiProject>();
    }

    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }
    
    public void addProject(BiProject project) {
        if (!getProjects().contains(project)) {
            getProjects().add(project);
        }
        if (!project.getEmployees().contains(this)) {
            project.getEmployees().add(this);
        }
    }

    public Collection<BiProject> getProjects() {
        return projects;
    }

    public String toString() {
        return "Employee id: " + getId() + " name: " + getName() + 
               " with " + getProjects().size() + " projects";
    }
}
