package com.oreilly.springdata.jpa.entity;

public enum EmployeeType {
    FULL_TIME_EMPLOYEE,
    PART_TIME_EMPLOYEE,  
    CONTRACT_EMPLOYEE 
}
