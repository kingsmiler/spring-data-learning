package com.oreilly.springdata.jpa.entity;

import javax.persistence.Entity;

@Entity
public class Department extends AbstractEntity {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String deptName) {
        this.name = deptName;
    }

    public String toString() {
        return "Department id: " + getId() +
                ", name: " + getName();
    }
}