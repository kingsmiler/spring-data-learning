package com.oreilly.springdata.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;


/**
 * 多对多双向关联的项目表
 */
@Entity
@Table(name = "bi_project")
public class BiProject extends AbstractEntity {
    protected String name;

    @ManyToMany(mappedBy = "projects")
    private Collection<BiEmployee> employees;

    public BiProject() {
        employees = new ArrayList<BiEmployee>();
    }


    public String getName() {
        return name;
    }

    public void setName(String projectName) {
        this.name = projectName;
    }

    public Collection<BiEmployee> getEmployees() {
        return employees;
    }

    public void addEmployee(BiEmployee employee) {
        if (!getEmployees().contains(employee)) {
            getEmployees().add(employee);
        }
        if (!employee.getProjects().contains(this)) {
            employee.getProjects().add(this);
        }
    }

    public String toString() {
        return "Project id: " + getId() + ", name: " + getName() +
                " with " + getEmployees().size() + " employees";
    }
}
