insert into Customer (id, email, firstname, lastname) values (1, 'dave@dmband.com', 'Dave', 'Matthews');
insert into Customer (id, email, firstname, lastname) values (2, 'carter@dmband.com', 'Carter', 'Beauford');
insert into Customer (id, email, firstname, lastname) values (3, 'boyd@dmband.com', 'Boyd', 'Tinsley');
insert into Customer (id, email, firstname, lastname) values (4, 'boyd@126.com', 'Boyd', 'Tinsley');
insert into Customer (id, email, firstname, lastname) values (5, 'xman@dmband.com', 'xman', 'wang');
insert into Customer (id, email, firstname, lastname) values (6, 'xman@126.com', 'xman', 'wang');
insert into Customer (id, email, firstname, lastname) values (7, 'xman@163.com', 'xman', 'wang');
insert into Customer (id, email, firstname, lastname) values (8, 'help@126.com', 'help', 'wang');
insert into Customer (id, email, firstname, lastname) values (9, 'help@163.com', 'help', 'wang');
insert into Customer (id, email, firstname, lastname) values (10, 'help@dmband.com', 'help', 'wang');

insert into Address (id, street, city, country, customer_id) values (1, '27 Broadway', 'New York', 'United States', 1);
insert into Address (id, street, city, country, customer_id) values (2, '27 Broadway', 'New York', 'United States', 1);

insert into Product (id, name, description, price) values (1, 'iPad', 'Apple tablet device', 499.0);
insert into Product (id, name, description, price) values (2, 'MacBook Pro', 'Apple notebook', 1299.0);
insert into Product (id, name, description, price) values (3, 'Dock', 'Dock for iPhone/iPad', 49.0);

insert into Product_Attributes (attributes_key, product_id, attributes) values ('connector', 1, 'socket');
insert into Product_Attributes (attributes_key, product_id, attributes) values ('connector', 3, 'plug');

insert into orders (id, customer_id, shippingaddress_id) values (1, 1, 2);

insert into line_item (id, product_id, amount, order_id, price) values (1, 1, 2, 1, 499.0);
insert into line_item (id, product_id, amount, order_id, price) values (2, 2, 1, 1, 1299.0);