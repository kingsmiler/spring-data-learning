CREATE DATABASE `springjpa`
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE USER 'springjpa'@'%'
  IDENTIFIED BY 'springjpa';

GRANT ALL PRIVILEGES ON springjpa.* TO 'springjpa'@'%';