package com.xman.spring.facility.repository.factory;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.io.Serializable;

/**
 * @author xman
 */
@SuppressWarnings("unchecked")
@NoRepositoryBean
public class EsRepositoryImpl<T, ID extends Serializable>
		extends SimpleJpaRepository<T, ID> implements EsRepository<T, ID> {

	private EntityManager entityManager;

	// There are two constructors to choose from, either can be used.
	public EsRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
		super(domainClass, entityManager);

		// This is the recommended method for accessing inherited class dependencies.
		this.entityManager = entityManager;
	}

	
	public Query createQuery(String sql)
	{
		return entityManager.createQuery(sql);
	}

}
