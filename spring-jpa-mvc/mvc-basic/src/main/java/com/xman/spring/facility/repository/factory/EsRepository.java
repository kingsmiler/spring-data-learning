package com.xman.spring.facility.repository.factory;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author xman
 */
@NoRepositoryBean
public interface EsRepository<T, ID extends Serializable>
		extends JpaRepository<T, ID> {

	Page<T> findAll(Specification<T> spec, Pageable pageable);
	
	Query createQuery(String sql);
}
