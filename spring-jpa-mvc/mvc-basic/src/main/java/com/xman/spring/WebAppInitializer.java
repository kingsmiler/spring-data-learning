package com.xman.spring;

import com.apress.springrecipes.court.service.config.ServiceConfiguration;
import com.xman.spring.config.ComponentConfig;
import com.xman.spring.config.DataConfig;
import com.xman.spring.config.MailConfig;
import com.xman.spring.config.MvcConfiguration;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;


public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{
                //数据库
                DataConfig.class,
                ServiceConfiguration.class,
                // 邮件配置
                MailConfig.class,
                ComponentConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{MvcConfiguration.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/", "/welcome"};
    }

    @Override
    protected Filter[] getServletFilters() {

        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");

        return new Filter[]{characterEncodingFilter};
    }

}
