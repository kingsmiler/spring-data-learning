package com.xman.spring.facility.entity;

public enum OwnerType {
    PROJECT,
    USER,
}
