
package com.xman.spring.facility.controller;

import com.xman.spring.facility.entity.Attachment;
import com.xman.spring.facility.repository.AttachmentDao;
import com.xman.spring.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.util.*;


@Controller
@RequestMapping("/attach")
public class AttachmentController {
    private static final Logger log = LoggerFactory.getLogger(AttachmentController.class);
    private static final String URL_PREFIX = "/attach";

    @Autowired
    private AttachmentDao attachmentDao;

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    @ResponseBody
    public Map list() {
        log.debug("uploadGet called");
        List<Attachment> list = attachmentDao.findAll();
        for (Attachment attach : list) {
            attach.setUrl("/picture/" + attach.getId());
            attach.setDeleteUrl(URL_PREFIX + "/delete/" + attach.getId());
            attach.setDeleteType("DELETE");
        }

        Map<String, Object> files = new HashMap<>();
        files.put("files", list);
        log.debug("Returning: {}", files);
        return files;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public Map upload(MultipartHttpServletRequest request) {
        log.debug("uploadPost called");
        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf;
        List<Attachment> list = new LinkedList<>();

        while (itr.hasNext()) {
            mpf = request.getFile(itr.next());
            log.debug("Uploading {}", mpf.getOriginalFilename());

            String newFilenameBase = UUID.randomUUID().toString();
            String originalFileExtension = mpf.getOriginalFilename().substring(mpf.getOriginalFilename().lastIndexOf("."));
            String newFilename = newFilenameBase + originalFileExtension;

            String contentType = mpf.getContentType();

            File newFile = new File(Constants.UPLOAD_DIR + newFilename);
            try {
                mpf.transferTo(newFile);

                Attachment attach = new Attachment();
                attach.setName(mpf.getOriginalFilename());
                attach.setContentType(contentType);
                attach.setSize(mpf.getSize());
                attach.setNewName(newFilename);
                attach = attachmentDao.save(attach);

                attach.setUrl("/picture/" + attach.getId());
                attach.setDeleteUrl(URL_PREFIX + "/delete/" + attach.getId());
                attach.setDeleteType("DELETE");

                list.add(attach);

            } catch (IOException e) {
                log.error("Could not upload file " + mpf.getOriginalFilename(), e);
            }
        }

        Map<String, Object> files = new HashMap<>();
        files.put("files", list);
        return files;
    }


    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public List delete(@PathVariable Long id) {
        Attachment attach = attachmentDao.findOne(id);
        File imageFile = new File(Constants.UPLOAD_DIR + "/" + attach.getNewName());
        imageFile.delete();

        attachmentDao.delete(attach);
        List<Map<String, Object>> results = new ArrayList<>();
        Map<String, Object> success = new HashMap<>();
        success.put("success", true);
        results.add(success);
        return results;
    }
}
