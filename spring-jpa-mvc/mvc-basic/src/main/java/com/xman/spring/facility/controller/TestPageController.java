package com.xman.spring.facility.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@RequestMapping
public class TestPageController {

    @RequestMapping("/hello/image")
    public String welcome(Model model) {
        Date today = new Date();
        model.addAttribute("today",today);
        return "/hello/image";
    }

    @RequestMapping("/up/file")
    public String testUploadFile() {

        return "/hello/file";
    }
}
