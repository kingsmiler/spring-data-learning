package com.xman.spring.config;


import com.xman.spring.utils.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import java.io.File;

@Component
@Import(PropertyPlaceholderConfig.class)
public class AppPathContextAware implements ServletContextAware {

    @Value("${file.upload.directory}")
    private String uploadPath;

    public void setServletContext(ServletContext servletContext) {
        Constants.APP_PATH = servletContext.getRealPath("/");

        Constants.UPLOAD_DIR = Constants.APP_PATH + uploadPath + "/";
        File uploadDir = new File(Constants.UPLOAD_DIR);
        if (!uploadDir.exists()) {
            uploadDir.mkdirs();
        }
        System.out.println("===============" + Constants.UPLOAD_DIR);
    }
}
