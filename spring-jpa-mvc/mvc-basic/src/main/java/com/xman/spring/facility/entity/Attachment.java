
package com.xman.spring.facility.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xman.spring.facility.entity.factory.AbstractEntity;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "attachments")
@JsonIgnoreProperties({"id", "contentType", "dateCreated", "lastUpdated"})
public class Attachment extends AbstractEntity {
    /**
     * 原文件名
     */
    private String name;
    /**
     * 新文件名
     */
    private String newName;
    /**
     * 文件内容类型
     */
    private String contentType;
    /**
     * 文件大小
     */
    @Column
    private Long size;
    /**
     * 文件创建时间
     */
    @Temporal(TemporalType.DATE)
    private Date dateCreated;
    /**
     * 文件更新时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    /**
     * 文件所有者类型，指文件是属于个人，还是其它等。
     */
    @Enumerated(EnumType.STRING)
    private OwnerType ownerType;

    /**
     * 文件的保存路径
     */
    private String path;

    /**
     * 文件的下载请求地址
     */
    @Transient
    private String url;

    /**
     * 文件的删除请求地址
     */
    @Transient
    private String deleteUrl;

    @Transient
    private String deleteType;

    public Attachment() {
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }



    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the size
     */
    public Long getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(Long size) {
        this.size = size;
    }


    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the lastUpdated
     */
    public Date getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the deleteUrl
     */
    public String getDeleteUrl() {
        return deleteUrl;
    }

    /**
     * @param deleteUrl the deleteUrl to set
     */
    public void setDeleteUrl(String deleteUrl) {
        this.deleteUrl = deleteUrl;
    }

    /**
     * @return the deleteType
     */
    public String getDeleteType() {
        return deleteType;
    }

    /**
     * @param deleteType the deleteType to set
     */
    public void setDeleteType(String deleteType) {
        this.deleteType = deleteType;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public OwnerType getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(OwnerType ownerType) {
        this.ownerType = ownerType;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
