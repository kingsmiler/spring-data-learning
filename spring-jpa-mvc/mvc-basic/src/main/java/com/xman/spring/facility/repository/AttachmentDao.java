
package com.xman.spring.facility.repository;

import com.xman.spring.facility.entity.Attachment;
import com.xman.spring.facility.repository.factory.EsRepository;


public interface AttachmentDao extends EsRepository<Attachment, Long> {

}
