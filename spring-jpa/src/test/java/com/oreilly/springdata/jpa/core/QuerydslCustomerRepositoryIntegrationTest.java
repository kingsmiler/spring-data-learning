/*
 * Copyright 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oreilly.springdata.jpa.core;

import com.mysema.query.types.Predicate;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration test showing the usage of Querydsl {@link com.mysema.query.types.Predicate} to query repositories implementing
 * {@link org.springframework.data.querydsl.QueryDslPredicateExecutor}.
 *
 * @author Oliver Gierke
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/spring-datasource.xml")
public class QuerydslCustomerRepositoryIntegrationTest {

    static final QCustomer qCustomer = QCustomer.customer;

    @Autowired
    DslCustomerRepository repository;

    @Test
    public void findProductsByQuerydslPredicate() {

        Iterable<Customer> customerIterable = repository.findAll(qCustomer.firstname.contains("xman"));

        Iterator<Customer> iterator = customerIterable.iterator();
        while (iterator.hasNext()) {
            Customer customer = iterator.next();
            System.out.println(customer.getLastname());
        }
    }

    @Test
    public void findProductsByQuerydslPredicate2() {
        CriteriaBuilder cb;
        final List<Predicate> predicates = new ArrayList<Predicate>();
        Predicate predicate = qCustomer.firstname.contains("xman");


        Iterable<Customer> customerIterable = repository.findAll(predicate);

        Iterator<Customer> iterator = customerIterable.iterator();
        while (iterator.hasNext()) {
            Customer customer = iterator.next();
            System.out.println(customer.getLastname());
        }
    }
}
