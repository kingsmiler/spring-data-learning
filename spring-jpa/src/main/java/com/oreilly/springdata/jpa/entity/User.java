package com.oreilly.springdata.jpa.entity;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
public class User extends AbstractEntity {

    @Basic(fetch = FetchType.EAGER)
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "login_id")
    private Set<LoginInfo> loginInfo;

    @Column(length = 32)
    private String introduce;

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Set<LoginInfo> getLoginInfo() {
        return loginInfo;
    }

    public void setLoginInfo(Set<LoginInfo> loginInfo) {
        this.loginInfo = loginInfo;
    }
}
