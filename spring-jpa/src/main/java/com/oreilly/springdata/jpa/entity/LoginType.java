package com.oreilly.springdata.jpa.entity;


public enum LoginType {
    NAME,
    EMAIL,
    PHONE
}
