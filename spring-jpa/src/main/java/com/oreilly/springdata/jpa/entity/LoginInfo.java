package com.oreilly.springdata.jpa.entity;


import javax.persistence.*;

@Entity
@Table(name = "user_login_info")
public class LoginInfo  extends AbstractEntity{

    @Column(length = 32)
    private String name;

    @Column(length = 64)
    private String email;

    @Column(length = 16)
    private String phone;

    @Basic(fetch = FetchType.EAGER)
    @OneToOne(mappedBy = "loginInfo")
    private User user;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
